<?xml version="1.0" encoding="UTF-8"?>
<tileset name="tiles_spritesheet2" tilewidth="70" tileheight="70" spacing="2" tilecount="156" columns="12">
 <image source="tiles_spritesheet.png" width="914" height="936"/>
 <tile id="9">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="20">
  <objectgroup draworder="index">
   <object id="1" x="0" y="-0.25" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="44">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="70" height="40.25"/>
  </objectgroup>
 </tile>
 <tile id="56">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="70" height="40.25"/>
  </objectgroup>
 </tile>
 <tile id="68">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="70" height="40.25"/>
  </objectgroup>
 </tile>
 <tile id="80">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="70" height="40"/>
  </objectgroup>
 </tile>
 <tile id="91">
  <objectgroup draworder="index">
   <object id="1" x="-0.125" y="0" width="70.125" height="69.5"/>
  </objectgroup>
 </tile>
 <tile id="103">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="115">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="70" height="69.5"/>
  </objectgroup>
 </tile>
 <tile id="140">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="151">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
 <tile id="152">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="70" height="70"/>
  </objectgroup>
 </tile>
</tileset>
