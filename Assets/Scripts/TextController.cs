﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{

    public static TextController instance;

    Queue<IEnumerator> queue = new Queue<IEnumerator>();

    Text text;
    public GameObject pressA;
    public GameObject textBox;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            text = GetComponent<Text>();
            textBox.SetActive(false);
            pressA.SetActive(false);
            text.text = "";
        }
        else
        {
            Destroy(gameObject);
        }
    }


    private void OnEnable()
    {
        StartCoroutine("ProcessQueueCoroutine");
    }

    private void Start()
    {

        TextController.TypeText("Here's some text boiiiii");
        TextController.WaitForInput();
        TextController.ClearText();
        TextController.TypeText("Yess You Waited");
        TextController.WaitForInput();
        TextController.ClearText();
        

    }



    IEnumerator ProcessQueueCoroutine()
    {
        while (enabled)
        {
            if (queue.Count > 0)
            {
                textBox.SetActive(true);
                //show text box
                while(queue.Count > 0)
                {
                    yield return StartCoroutine(queue.Dequeue());
                }
                textBox.SetActive(false);
                //hide text box
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public static void ClearText()
    {
        instance.queue.Enqueue(instance.ClearTextCoroutine());
    }

    IEnumerator ClearTextCoroutine()
    {
        text.text = "";
        yield return null;
    }

    public static void TypeText(string str)
    {
        instance.queue.Enqueue(instance.TypeTextCoroutine(str));
    }

    IEnumerator TypeTextCoroutine(string text1)
    {
        for (int i = 0; i < text1.Length; i++)
        {
            this.text.text = text1.Substring(0, i + 1);
            yield return new WaitForSeconds(.2f);
        }
    }

    public static void WaitForInput()
    {
        instance.queue.Enqueue(instance.WaitForInputCoroutine());
    }

    IEnumerator WaitForInputCoroutine()
    {
        aPressed = false;
        pressA.SetActive(true);
        while (!aPressed)
        {
            yield return new WaitForEndOfFrame();
        }
        pressA.SetActive(false);
    }

    bool aPressed = false;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A)) aPressed = true;
    }

}
