﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{

    PlayerController instance;

    public int sceneBuildIndex = 0;

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;

    public GameObject cameraParent;

    public GameObject gameManagerObject;
    GameManager gameManager;
    AudioManager audioManager;

    Scene sceneToLoad;



    SpriteRenderer sprite_renderer;

    public Sprite[] sprites;

    public Sprite[] walkingSprites;

    public Sprite[] teteringSprites;

    Sprite idleSprite;
    Sprite jumpSprite;
    Sprite fallSprite;
    Sprite duckSprite;
    Sprite hurtSprite;

    private bool grounded = false;
    private bool circleGrounded = false;

    bool downPressed = false;

    bool doingKnockback = false;

    private Rigidbody2D rb2d;

    EndLevelObject endlevelobject;
    public GameObject endpoint;

    PlayerHealthController playerHealthController;

    public ParticleSystem playerHitParticles;

    // Use this for initialization
    void Awake()
    {

        rb2d = GetComponent<Rigidbody2D>();
        sprite_renderer = GetComponent<SpriteRenderer>();
        endlevelobject = endpoint.GetComponent<EndLevelObject>();
        gameManager = gameManagerObject.GetComponent<GameManager>();
        audioManager = gameManagerObject.GetComponent<AudioManager>();
        playerHealthController = GetComponent<PlayerHealthController>();

        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        idleSprite = sprites[0];
        jumpSprite = sprites[1];
        fallSprite = sprites[2];
        duckSprite = sprites[3];
        hurtSprite = sprites[5];
        
        StartCoroutine("Animations");
        StartCoroutine("sounds");

    }

    // Update is called once per frame
    void Update()
    {
        

        if (endlevelobject.levelOver) return;
        circleGrounded = Physics2D.CircleCast(transform.position,0.651f, Vector2.down, -0.651f, 1 << LayerMask.NameToLayer("Ground"));

        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if (Input.GetButtonDown("Jump") && grounded)
        {
            jump = true;
        }

        if(Input.GetAxis("Vertical") == -1f)
        {
            downPressed = true;
        }
        else
        {
            downPressed = false;
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");

        if (doingKnockback) return;

        if (endlevelobject.levelOver) return;

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
        {
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
            //Debug.Log("Clamping your speed bro");
        }

        if (h == 0f)
        {
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        if (jump)
        {
            
            rb2d.AddForce(new Vector2(0f, jumpForce));
            AudioManager.PlayVariedEffect("jumppp11");
            jump = false;
        }

        
    }


    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }


    IEnumerator Animations()
    {

        while (true)
        {

            if (grounded)
            {
                if(rb2d.velocity.x > 0.1 || rb2d.velocity.x < -0.1)
                {
                    
                    for (int i = 0; i < walkingSprites.Length; i++)
                    {
                        if (!grounded || endlevelobject.levelOver)
                        {
                            //Debug.Log("Broke Walk Cycle");
                            break;
                        }
                        
                        sprite_renderer.sprite = walkingSprites[i];
                        
                        //Debug.Log("Walk Sprite Frame" + i); 
                        yield return new WaitForSeconds(.15f);
                    }
                    yield return new WaitForEndOfFrame();
                }
                else if(downPressed == true)
                {
                    sprite_renderer.sprite = duckSprite;
                    yield return new WaitForEndOfFrame();
                }
                else if (endlevelobject.levelOver == true)
                {
                    sprite_renderer.sprite = sprites[4];
                    rb2d.velocity = Vector3.zero;
                    Debug.Log("YASS YOU WON");
                    yield return new WaitForEndOfFrame();
                    yield return new WaitForSeconds(8);
                    SceneManager.LoadScene(sceneBuildIndex, LoadSceneMode.Single);
                }

                else
                {
                    sprite_renderer.sprite = idleSprite;
                    yield return new WaitForEndOfFrame();
                }
            }

            else if(circleGrounded && !grounded)
            {
                for(int i = 0; i < teteringSprites.Length; i++)
                {
                    if (grounded) break;
                    sprite_renderer.sprite = teteringSprites[i];

                        
                    yield return new WaitForSeconds(.15f);
                }
            }
            else
            {
                
                if(rb2d.velocity.y < 0)
                {
                    sprite_renderer.sprite = fallSprite;
                    //Debug.Log("Falling!");
                    yield return new WaitForEndOfFrame();
                }
                else
                {
                    sprite_renderer.sprite = jumpSprite;
                    //Debug.Log("Jumped!");
                    yield return new WaitForEndOfFrame();
                }
               
            }
        }       
    }

    IEnumerator sounds()
    {

        while (true)
        {

            if (grounded)
            {
                if (rb2d.velocity.x > 0.1 || rb2d.velocity.x < -0.1)
                {

                    
                   
                        

                        
                        AudioManager.PlayVariedEffect("step_cloth2");
                        
                        yield return new WaitForSeconds(.3f);
                    
                }
                else if (downPressed == true)
                {
                    
                    yield return new WaitForEndOfFrame();
                }
                else if (endlevelobject.levelOver == true)
                {
                   
                    yield return new WaitForEndOfFrame();
                }

                else
                {
                    
                    yield return new WaitForEndOfFrame();
                }
            }
            else
            {
                if (rb2d.velocity.y < 0)
                {
                    sprite_renderer.sprite = fallSprite;
                    //Debug.Log("Falling!");
                    yield return new WaitForEndOfFrame();
                }
                else
                {
                    //AudioManager.PlayEffect("jumppp11");
                    //Debug.Log("Jumped!");
                    yield return new WaitForSeconds(1);
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if (collision.gameObject.tag == "Enemy")
        //{
        //    knockDir = (Vector2)(transform.position - collision.transform.position) + Vector2.up;
        //    doingKnockback = true;
        //    playerHitParticles.transform.position = transform.position/*collision.transform.position + new Vector3(1f, 0, 1)*/;
        //    playerHitParticles.Play();

            


        //    playerHealthController.lives -= 1;
        //}
    }

    public void KnockBack(Vector2 knockDir)
    {
        playerHitParticles.transform.position = transform.position;
        playerHitParticles.Play();
        StartCoroutine("KnockBackCoroutine", knockDir);
    }

    IEnumerator KnockBackCoroutine (Vector2 knockDir) { 
        doingKnockback = true;
        Debug.Log("KnockBack = " + knockDir);
        rb2d.AddForce(knockDir, ForceMode2D.Impulse);
        yield return new WaitForSeconds(.5f);
        doingKnockback = false;

    }
    public void Knockup(Vector2 knockDir)
    {
        StartCoroutine("KnockupCoroutine", knockDir);
    }

    IEnumerator KnockupCoroutine(Vector2 knockDir)
    {
        
        Debug.Log("KnockBack = " + knockDir);
        rb2d.AddForce(knockDir, ForceMode2D.Impulse);
        yield return new WaitForSeconds(.5f);
        

    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawLine(transform.position, transform.position + Vector3.right);
    //}

}





