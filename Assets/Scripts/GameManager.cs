﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    GameManager instance;

    public GameObject endLevelObject;

    EndLevelObject endPoint;

    PlayerHealthController playerHealth;

    public Text gameOverUI;

    public GameObject player;

    public CountDownTimer timer;

    private void Awake()
    {
        if (instance == null)
        {

            instance = this;

        }
        else
        {
            Debug.Log("oh no");
        }

        gameOverUI.gameObject.SetActive(false);

        
    }

    // Use this for initialization
    void Start ()
    {
        playerHealth = player.GetComponent<PlayerHealthController>();
        endPoint = endLevelObject.GetComponent<EndLevelObject>();
        StartCoroutine("GameOver");
        
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (playerHealth.lives < 0 )
        {
            gameOverUI.text = "Game OVER";
            gameOverUI.gameObject.SetActive(true);
        }

        

        if(endPoint.levelOver == true)
        {
            gameOverUI.text = "You Win";
            gameOverUI.gameObject.SetActive(true);
            //player.GetComponent<SpriteRenderer>().sprite = player.GetComponent<PlayerController>().sprites[4];
        }
	}

    IEnumerator GameOver()
    {
        while (true)
        {
            if (playerHealth.lives < 0)
            {
                yield return new WaitForSeconds(6);
                SceneManager.LoadScene(0);
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
