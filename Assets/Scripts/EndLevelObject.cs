﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevelObject : MonoBehaviour {

    public bool levelOver = false;

    public float itemHeight = 3f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //win
            transform.position = collision.gameObject.transform.position + new Vector3(0, itemHeight, 0);
            levelOver = true;
            
        }
    }
}
