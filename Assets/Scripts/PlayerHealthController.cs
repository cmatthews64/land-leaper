﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthController : MonoBehaviour {

    public int health = 2;
    public int lives = 3;

    Transform player;
    public Transform fallDeath;
    public Transform respawnLocation;

    public Canvas canvas;
    public Image livesUI;
    public Text gameOverUI;

    public GUITexture liveGUI;

    

    public Sprite[] livesArray;


    private void Awake()
    {
        player = GetComponent<Transform>();
        
        
    }
    // Use this for initialization
    void Start () {
        Debug.Log("Lives :" + lives);
        livesUI.sprite = livesArray[lives];
	}
	
	// Update is called once per frame
	void Update () {
		if(player.position.y < fallDeath.position.y)
        {
            Die_Reset();
            
        }
	}

    void Die_Reset()
    {
        lives -= 1;

        if (lives < 0) livesUI.sprite = livesArray[0];
        else livesUI.sprite = livesArray[lives];



        
        
        if (lives > -1)
            player.position = respawnLocation.position;
        //else gameOverUI.gameObject.SetActive(true);



        Debug.Log("Lives :" + lives);
    }

    
}
