﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    [HideInInspector] public bool facingRight = true;

    public Transform groundCheck;
    private bool grounded = false;

    Rigidbody2D rb2d;

    public float moveDist = 5f;

    public ParticleSystem deathParticles;

    private void Awake()
    {
        
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start()
    {
        
        StartCoroutine("enemyMovement");
    }

    // Update is called once per frame
    void Update()
    {
        grounded = Physics2D.Linecast(transform.position, (transform.position - new Vector3(0,4,0)), 1 << LayerMask.NameToLayer("Ground"));
    }



    IEnumerator enemyMovement()
    {
        
            

                Vector3 originalPosition = transform.position;
                Vector3 farRightPosition = transform.position + Vector3.right * moveDist;
                while (true)
                {
                    for (float i = 0; i < moveDist; i += Time.deltaTime)
                    {
                        transform.position = Vector3.Lerp(originalPosition, farRightPosition, i / moveDist);
                        yield return new WaitForEndOfFrame();
                    }

                    Flip();

                    for (float i = 0; i < moveDist; i += Time.deltaTime)
                    {
                        transform.position = Vector3.Lerp(farRightPosition, originalPosition, i / moveDist);
                        yield return new WaitForEndOfFrame();
                    }

                }
           
        

    }
    void Flip()
    {
        facingRight = !facingRight;
        if (facingRight) transform.right = Vector3.right;
        else transform.right = -Vector3.right;
    }

    public void Die()
    {
        deathParticles.transform.position = gameObject.transform.position - new Vector3(0,1,0);
        deathParticles.Play();
        this.gameObject.SetActive(false);
        Debug.Log("TODO: Add Death Particle Effects");
    }

    private void OnDisable()
    {
        deathParticles.transform.position = gameObject.transform.position - new Vector3(0, 1, 0);
        deathParticles.Play();
    }
}
