﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimator : MonoBehaviour
{
    SpriteRenderer spriteRend;
    public Sprite[] sprites;

    private void Awake()
    {
        spriteRend = GetComponent<SpriteRenderer>();
    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine("enemyAnim");
    }

    

    IEnumerator enemyAnim()
    {
       
        while (true)
        {
            for (int i = 0; i < sprites.Length; i++)
            {
                spriteRend.sprite = sprites[i];
                yield return new WaitForSeconds(.15f);
            }
        }

    }

    
}

