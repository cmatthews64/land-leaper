﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeathController : MonoBehaviour {
    

    private void Awake()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayerController playerController = collision.gameObject.GetComponent<PlayerController>();
            Debug.Log("Collided With Player");
            foreach(ContactPoint2D cp in collision.contacts)
            {
                float angle = Vector2.Angle(cp.normal, Vector2.down);
                

                if (angle < 45)
                {
                    Debug.Log("TODO: implement enemy kill");
                    Vector2 knockDir = -cp.normal * 30;
                    playerController.Knockup(knockDir);
                    gameObject.SetActive(false);
                    //destroy enemy
                }
                else
                {
                    //collision.gameObject.GetComponent<Rigidbody2D>().AddForce((Vector2)(transform.position + collision.transform.position) + Vector2.up * 15, ForceMode2D.Impulse);//hurt player
                    Vector2 knockDir = -cp.normal * 5 + Vector2.up * 8;
                    playerController.KnockBack(knockDir);
                }
            }
            

        }
    }

    
    //IEnumerator KnockBack(Rigidbody2D rb2d)
    //{
    //    rb2d.AddForce(knockDir * 15, ForceMode2D.Impulse);
    //    yield return new WaitForSeconds(1);
        
    //}
}
