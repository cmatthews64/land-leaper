﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdLevelCameraController : MonoBehaviour {
    public Transform target;

    public Vector3 offset = new Vector3(0, 10, 0);

    public float camMinXPosition = -100;
    public float camMaxXPosition = 100;

    public float camMinYPosition = -100;
    public float camMaxYPosition = 100;




    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(target.position.x + offset.x, camMinXPosition, camMaxXPosition), Mathf.Clamp(target.position.y + offset.y, camMinYPosition,camMaxYPosition), offset.z);
    }
}
