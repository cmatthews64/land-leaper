﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlahCameraController : MonoBehaviour {

    public Transform target;

    public Vector3 offset = new Vector3(0, 10, 0);

    public GameObject minXPosition;
    public GameObject maxXPosition;




    void Update () {
        transform.position = new Vector3(Mathf.Clamp(target.position.x, minXPosition.transform.position.x, maxXPosition.transform.position.x) + offset.x, offset.y, offset.z);
    }
}
