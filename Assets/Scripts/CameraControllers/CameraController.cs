﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;

    public Vector3 offset = new Vector3(0,10,0);

    public Vector3 mazeOffset = new Vector3(0, 0, 0);

    public GameObject MazeStart;

    public GameObject CameraAnchor;

    public GameObject mazeFinish;

    public GameObject StartPartPosition;

    public float startPartcamClampMinX = -10;
    public float startPartcamClampMaxX = 10;

    public float camClampMinY = -10;
    public float camClampMaxY = 10;

    public float camClampMinX = -10;
    public float camClampMaxX = 10;

    Camera camera;

	// Use this for initialization
	void Start () {
        camera = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {

        if (target.transform.position.x > MazeStart.transform.position.x && target.transform.position.y > MazeStart.transform.position.y)
        {
            //in maze
            
            transform.position =Vector3.Lerp(transform.position, CameraAnchor.transform.position + mazeOffset, 2f * Time.deltaTime);
            if(target.transform.position.x > mazeFinish.transform.position.x && target.transform.position.y > mazeFinish.transform.position.y)
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(target.position.x, target.position.y + offset.y, offset.z), 2f * Time.deltaTime);
            }
        }
        else if (target.transform.position.x < StartPartPosition.transform.position.x && target.transform.position.y > StartPartPosition.transform.position.y)
        {
            //start part
            transform.position = new Vector3(Mathf.Clamp(target.position.x, startPartcamClampMinX, startPartcamClampMaxX), Mathf.Clamp(target.position.y + offset.y, camClampMinY, camClampMaxY), offset.z);
        }
        else
        {

            transform.position = new Vector3(Mathf.Clamp(target.position.x, camClampMinX, camClampMaxX), Mathf.Clamp(target.position.y + offset.y, camClampMinY, camClampMaxY), offset.z);
        }
	}
}
