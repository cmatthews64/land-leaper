﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDownTimer : MonoBehaviour {

    CountDownTimer instance2;

    public float timeLeft = 10f;

    Text timerText;

    private void Awake()
    {
        if (instance2 == null)
        {
            instance2 = this;
        }
        timerText = GetComponent<Text>();
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timeLeft -= Time.deltaTime;

        int timeLeftInt = (int)timeLeft;

        timerText.text = "Time Left: " + timeLeftInt;
	}

    public float GetTimeLeft()
    {
        return timeLeft;
    }
}
